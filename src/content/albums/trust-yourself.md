---
title: Trust Yourself
description: A psychedelic adventure to beyond the horizon.
pubDate: 2018-05-12
backgroundImage: "/backgrounds/70s.jpg"
tags: ["album"]
---

Trust Yourself is my first true album.

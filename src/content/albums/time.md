---
title: Time
description: Time is a flat circle.
pubDate: 2024-03-12
backgroundImage: "/backgrounds/fractal.png"
tags: ["album", "remaster"]
---

Time was originally an unreleased album and remained lost for years.
---
title: Condominium
description: Crazy synth times in Sheffield.
pubDate: 2024-03-12
backgroundImage: "/backgrounds/city.jpg"
tags: ["album", "sound-design"]
---

Condomium was originally the name for the same album by me, Trust Yourself.
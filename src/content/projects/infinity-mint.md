---
title: Infinity Mint
description: A prototype Web3 Framework.
pubDate: 2023-12-17
tags: ["general", "infinity-mint"]
---

Infinity Mint was, and is, a prototype Web3 framework allowing you to build fun applications on the Web3 platform.

---
title: Lyds Text Adventure
description: ASCII based top-down roleplaying game
pubDate: 2023-12-17
tags: ["general", "games"]
---

LydsTextAdventure is a lost video game I was working on. I don't have the source code for it anymore.

Jellica, an awesome IDM producer, created a few tracks for this game.
